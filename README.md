Octave Chooser
==============

This is a simple shim to run a particular version of GNU Octave.

Octave normally installs itself as a set of versioned executables, along
with symbolic links to the most recently installed version. When
installing and testing multiple versions of Octave at once, it can be
easy to use the wrong version without noticing.

This script is similar to pyenv or qtchooser, allowing the user to
select the one version of Octave that will be the default, while
allowing other versions to be coinstalled.

License
-------

This project is licensed under the
[GPL](https://www.gnu.org/licenses/gpl.html), version 3 or later. See
[COPYING](COPYING) for the full license text.
