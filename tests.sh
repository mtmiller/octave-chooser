#!/bin/sh
#
# tests.sh - Octave Chooser unit test suite
#
# Copyright (C) 2019 Mike Miller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Use the default shunit2 if installed on $PATH
: ${SHUNIT=shunit2}

# The default version that is hardcoded in octave-chooser for now
default_version="4.4.1"

# List of versions to test
versions="${default_version} 5.1.0 6.2.0"

test_default_version ()
{
  output=$(octave --version)
  assertEquals "$default_version" "$output"
}

test_alternate_versions ()
{
  for v in $versions; do
    output=$(OCTAVE_VERSION=$v octave --version)
    assertEquals "$v" "$output"
  done
}

test_octave_chooser_vars ()
{
  unset OCTAVE_HOME
  unset OCTAVE_VERSION
  eval $(octave-chooser --vars)
  assertEquals "$SHUNIT_TMPDIR" "$OCTAVE_HOME"
  assertEquals "$default_version" "$OCTAVE_VERSION"
}

oneTimeSetUp ()
{
  bindir="${SHUNIT_TMPDIR}/bin"
  mkdir -p ${bindir}
  install octave-chooser ${bindir}/octave
  install octave-chooser ${bindir}/octave-chooser
  for v in $versions; do
    printf "#!/bin/sh\\necho ${v}\\n" > ${bindir}/octave-${v}
    chmod +x ${bindir}/octave-${v}
  done
  export PATH="${bindir}:${PATH}"
}

. $SHUNIT
